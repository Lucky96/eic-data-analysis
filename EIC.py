import pandas as pd              #importing all the libraries
import numpy as np
from pandas import ExcelWriter
import datetime
import matplotlib.pyplot as plt

df = pd.read_excel("/Users/prakharchaturvedi/Desktop/project_final/Settlement.xlsx") #reading the file, specify location from your directory

df.head()

print(df['SETTLEMENT'].isnull().sum()) #checking for null values

df.drop(['LEVEL', 'NOTE', 'REVISION'], axis = 1, inplace = True) #dropping columns which are not required

df.head()

hard_date = datetime.date(2014 ,1, 23) #set the starting date
df['DATETIME']= pd.to_datetime(df['DATETIME']) #convert datetime to numpy datetime
df['days'] = df['DATETIME'] - np.datetime64('2014-01-23')
df.days = df.days.dt.days # extract the days column

df.set_index('days', inplace=True)
df.groupby('NAME')['SETTLEMENT'].plot(legend=True) #plot all graphs together

df_by_id = df.groupby('NAME')
df_by_id.describe().sum()
id1 = list(df_by_id)[0]
df_by_id.plot(x = 'days', y = 'SETTLEMENT')  #Plot all graphs differently

df1 = df.groupby('NAME').apply(lambda x : x.set_index('DATETIME')
                                            .resample('D')
                                            .first()
                                            .interpolate()).reset_index(level=0, drop=True).reset_index()
#resample and add the the missing days. Interpolate on mssing values 


df1['Location'] = df1['NAME'].str[3:8]
df1['Sensor'] = df1['NAME'].str[12:]
df1["NAME"].fillna( method ='ffill', inplace = True) 
df1["Location"].fillna( method ='ffill', inplace = True) 
df1["Sensor"].fillna( method ='ffill', inplace = True) 
#extract sensor number and location name

df1.to_excel("/Users/prakharchaturvedi/Desktop/Project/S1.xlsx") #save the data


df2 = pd.read_excel("/Users/prakharchaturvedi/Desktop/project_final/Water_level.xlsx") #read water level data
df2.head()

df2.drop(['REVISION','READING (Hz)','TEMPERATURE','PRESSURE (kPa)','CHANGE IN PRESSURE (kPa)', 'NOTE'], axis = 1, inplace = True)

df2.head()

df2['days'] = df2['DATETIME'] - np.datetime64('2014-01-09')
df2.days = df2.days.dt.days

df2.head()
indexNames = df2[df2['days'] < 0 ].index
 
# Delete these row indexes from dataFrame
df2.drop(indexNames , inplace=True)

df2['Location'] = df2['NAME'].str[3:8]

df2['NAME'] = df2['NAME'].str.replace('VWP','SP')

df2.set_index('days', inplace=True)
df2.groupby('NAME')['WATER LEVEL'].plot(legend=True) #plot water level data

df2_by_id = df2.groupby('NAME')
df2_by_id.describe().sum()
id2 = list(df2_by_id)[0]
df2_by_id.plot(x = 'days', y = 'WATER LEVEL') #plot 77 data graphs differently

#add missing days and interpolate on the values
df2 = df2.groupby('NAME').apply(lambda x : x.set_index('DATETIME')
                                            .resample('D')
                                            .first()
                                            .interpolate()).reset_index(level=0, drop=True).reset_index()

#fill up the missing  values
df2["Location"].fillna( method ='ffill', inplace = True) 
df2["NAME"].fillna( method ='ffill', inplace = True) 

df1.to_excel("/Users/prakharchaturvedi/Desktop/Project/S1.xlsx")#save to excel file

df2['NAME'] = df2['NAME'].str.replace('NE01','NE02')#joining on NE02 sensor
df2['Sensor'] = df2['NAME'].str[12:]

df3 = pd.merge(df1, df2, on=['Location', 'days','Sensor'], how='inner')#join two dataframes

dfb = pd.read_excel("/Users/prakharchaturvedi/Desktop/project_final/Book2.xlsx") #read the book2 data which contains which has merged dataframe

#convert non stationary time sries to stationary time series
from statsmodels.tsa.stattools import adfuller
from numpy import log
series = pd.read_excel("/Users/prakharchaturvedi/Desktop/project_final/Book2.xlsx")
X = series.values
X = log(X)

dataset = pd.DataFrame({'WATER LEVEL': X[:, 0], 'SETTLEMENT': X[:, 2]})
dataset.head()

w = dataset["WATER LEVEL"].tolist()
s = dataset["SETTLEMENT"].tolist()

#find cross corelation and plot it
import matplotlib.pyplot as plt
fig, [ax1, ax2] = plt.subplots(2, 1, sharex=True)
ax1.xcorr(w, s, usevlines=True, maxlags=20, normed=True, lw=2)
ax1.grid(True)
ax2.acorr(w, usevlines=True, normed=True, maxlags=20, lw=2)
ax2.grid(True)

plt.show()






